;# Copyright (C) 2013-2018 �������� ���������(FoolQuest) e-mail:sboku@pisem.net 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
#
set font_size_plus 3
font configure TkDefaultFont -weight bold  ;# ����� ����� ��� ����������. 
font configure TkDefaultFont -size [expr $font_size_plus + [font configure TkDefaultFont -size]]

set bgcolor1 SystemButtonFace
set bgcolor2 yellow
set gametime0 0

set cmine "@"  
           
set dimensions {4 4 6} ;# default value
set mines 12           ;# default value

set minelist {}    ;# ������ ����� � ������
set omlist {}      ;# ������ ���������� ���
set oclist {}      ;# ������ �������� �����(��� ���)
set gametime 0
set errors 0       ;# Errors in current game
set hints 0        ;# The number of hints which you have taken
;#####################################################
;########## ��������� ������ ���������� ##############
;#####################################################
proc intseq {n} { ;#    intseq ���������� ������(������������������) �� n ����� ����� �� 0 �� n-1
  set r -1
  return [lmap x [lrepeat $n 1] { incr r }]
}

proc randn {n} { ;# return random integer in range [0 .. n-1]
  return [expr int($n*rand())] 
}

proc lrandn {n} { ;# Return random mix of [0 .. n-1]
  set iterations 10000
  set res [intseq $n]
  for {set i 0} {$i < $iterations} {incr i} {
    set n1 [randn $n]
    set res [lreplace [lappend res [lindex $res $n1]] $n1 $n1]   ;# ���������� ��������� ������� � �����
  }
  return $res 
}
;######################################################
;###### ��������� ��������� � ����������� ���� ########
;######################################################
#��������! ����� ������� "source" ������� [info script] 
#����� ��������� �������� ������, ��������, ������ ������
#�� ���� ������� �������� ��� ������� ����� � ���������� ����������.
set scriptname [file normalize [info script]]  ;# ��� �������

proc workdir {} {
  global scriptname
  return [file dirname $scriptname] ;# ��� ���������� �� ��������
}

proc autosavename {} { return [file join [workdir] autosave.sav] }

proc vcode {args} {
  foreach v $args {
    global $v
    append res "global $v; set $v [set $v]\n"
  }
  return $res
}

proc lcode {args} {
  foreach v $args {
    global $v
    append res "global $v; set $v \{[set $v]\}\n"
  }
  return $res
}

proc save_game_to_file {fname} { 
  set out [open $fname w]
  puts $out [vcode  mines gametime errors hints ] 
  puts $out [lcode  dimensions minelist omlist oclist  ] 
  close $out             
}

proc load_game_from_file {fname} { 
  global gametime0 gametime 
  source $fname
  set gametime0 [expr [clock seconds] - $gametime]
}

proc load_game_from_autosave {} { load_game_from_file [autosavename] }

proc save_game_to_autosave {} { save_game_to_file [autosavename] }

proc ft {tstr args} { return [format $tstr {*}$args] }       ;# ��������1 - ���������� � �������������������

;#####################################################
;############## ��������� ��� HINT ###################
;#####################################################
proc lunique1 {li} { ;# ��������� ������� ������ ��������� � �� �������������� ������� � [lsort -unique ...]!!
  set di [dict create]    ;# ��� ���� ������ �� ������ ������ ������ ������
  foreach tv $li { dict set di $tv "" }
  return [dict keys $di]
}

proc lunique {li} { return [lsort -unique $li] }

proc sets {option li1 li2} {
  set procname [lindex [info level 0] 0]
  switch -exact -- $option {
    difference {    
      foreach tv [lunique $li1] { set a($tv) ""}
      foreach tv [lunique $li2] { unset -nocomplain a($tv)}
      return [array names a]
    }
    union          { return [lunique [concat $li1 $li2]] }
    symmdifference { return [concat [$procname difference $li1 $li2] [$procname difference $li2 $li1]] }
    intersect      { return [$procname difference $li1 [$procname difference $li1 $li2]] }
    issubsetof     { return [expr ([llength [$procname difference $li1 $li2]] == 0)?1:0] }
    equals         { return [expr ([llength [$procname symmdifference $li1 $li2]] == 0)?1:0] }
    default        { error [ft "Error in procedure: %s" [info level 0]] }
  }
}

proc bigHint { {mode "biglist"} {imediatelen 25} } {
#puts "---- Mines ---- : ---- Empty ---- "
  global minelist omlist oclist    
  set omli $omlist
  set ocli $oclist
  set limit [dsize]
  set biglist {}    ;# ������ ������ ���������, ������� ����� ������
# "����������" ������ ������ (��� ����� ������ ������ �������� ��� ����)
  set d [sets difference $omli $minelist ]
  if {[llength $d] > 0} {
    if {$mode eq "shortlist"} {; return [list [list {} [lsort -integer $d]]]; }  ;# ��������� ����� �� mode          ;#   return !!
    foreach x $d { set omli [lsearch -not -all -inline $omli $x] } ;# ������� �� ������ omli ������������ ������      
  }
  set subsetlist [lmap x $ocli {neighborhood $x}]  ;# �������� ��� ������ subsetlist
  while 1 {
#  ���������� ������ subsetlist 
    set tset [concat $ocli $omli] 
    set subsetlist [lmap x $subsetlist {sets difference $x $tset}]   
    set subsetlist [lsearch -not -all -inline $subsetlist {}]      ;# ������� �� ������ ��� ������ ���������
    set subsetlist [lmap x $subsetlist {lsort -integer $x}]        ;# ��� ������ � lunique
    set subsetlist [lunique $subsetlist]                                   ;#!!!!!!!!!!!!!! lunique -> lunique1 - �����  ??????????????????
    set hintlist {}
# ���������� ������ subsetlist � ��������� ��������� 
    set y 0
    while { $y < [llength $subsetlist] } {
      set sy [lindex $subsetlist $y]
      if {0 == [minesinlist $sy]} {; lappend hintlist [list {} $sy]; }                  ;# ���������� ������ ���������
      if {[llength $sy] == [minesinlist $sy]} {; lappend hintlist [list $sy {} ]; }     ;# ���������� ������ ���������
      for {set x 0} {$x < $y} {incr x} {
        set sx [lindex $subsetlist $x]
        set isect [sets intersect $sx $sy]
        if {[llength $isect] == 0} {; continue; }
        foreach s1 [list  $sx $sy] s2 [list  $sy $sx] {    ;# ���� �� ������������ ��������
          if {[sets issubsetof $s2 $s1]} {
            set d1 [sets difference $s1 $s2]
            set d1 [lsort -integer $d1]
            if {([llength $d1] > 0) && ($d1 ni $subsetlist)} {
              lappend subsetlist $d1                                          ;# ���������� ������ subsetlist
              set m [minesinlist $d1]                                                     
              if {$m == 0} {; lappend hintlist [list {} $d1 ]; }                        ;# ���������� ������ ���������
              if {$m == [llength $d1]} {; lappend hintlist [list $d1 {} ]; }            ;# ���������� ������ ���������
            }
          } else {
            set d1 [sets difference $s1 $s2]
            set d2 [sets difference $s2 $s1]
            set mm [expr [minesinlist $s1] - [minesinlist $s2]]
            if {[llength $d1] == $mm} {
              lappend hintlist [list [lsort -integer $d1] [lsort -integer $d2]]         ;# ���������� ������ ���������
              foreach s [list $d1 $d2 $isect] {
                set s [lsort -integer $s]
                if {([llength $s] > 0) && ($s ni $subsetlist)} {
                  lappend subsetlist $s                                       ;# ���������� ������ subsetlist
                }
              }
            }
          } 
        }
      }
      set hintlist [lunique $hintlist] 
      if {($mode eq "shortlist") && ([llength $hintlist] > 0)} {; return $hintlist; }  ;# ��������� ����� �� mode    ;#   return !!
      if {[llength $hintlist] > $imediatelen} {; break; } 
      incr y
    }  ;# while { $y < [llength $subsetlist] }
# ----- ��������� ������������� 
    if { [llength $hintlist] == 0 } {
      set tset {}
      foreach x $subsetlist { set tset [sets  union $tset $x] }
      set m1 [minesinlist $tset]                               ;# ������� ��� "������ � ��� �����"
      set m2 [expr [llength $minelist] - [llength $omli]]      ;# ������� ��� �������� �����������
      if {$m1 == $m2} { 
        set hintlist [finalhint $hintlist $subsetlist $omli $ocli] 
      }
      if {($mode eq "shortlist") && ([llength $hintlist] > 0)} {; return $hintlist; }  ;# ��������� ����� �� mode    ;#   return !!
    }                          

    if { [llength $hintlist] == 0 } { return $biglist }                                                              ;#   return !!

    lappend biglist {*}$hintlist

    set arch_ocli $ocli   ;# ����������� ���� ����
# ��������� ����������� ���������
    foreach x [lunique $hintlist] {
      lassign $x om oc 
      lappend omli {*}$om   
      set ocli [oclist_append $ocli {*}$oc]
    }
    set omli [lunique $omli]
# ----- ��������� ��������� 

#  ������ ����� �������� ��� ������ subsetlist
    set li [lmap x [sets difference $ocli $arch_ocli] {neighborhood $x}] ;# ����� ����������
## ������� ����� ���������� � ������ subsetlist 
#    set subsetlist [lappend li {*}$subsetlist]
# ������� ����� ���������� � ����� subsetlist  (������, ����������� ��� �������, ��������� ����� ���������� � ������ ��� � �����)
    lappend subsetlist {*}$li
  } ;# while 1
}

proc finalhint {hli ssli omli ocli } {  ;# ����� ���������� ��� �������� ����, � ���������� ����� ���������� �����,
                                        ;# ����� ����������� �������� ��������� ������ ��������...
  global minelist omlist oclist 
  set hcli [sets difference [intseq [dsize]] [concat $omli $ocli]] ;# ������ �������� �����
  set hc [llength $hcli]
  set hm [expr [llength $minelist] - [llength $omli]]              ;# ���-�� ��� � �������� �������

  set y 0
  while { $y < [llength $ssli] } {
    set sy [lindex $ssli $y]
    for {set x 0} {$x < $y} {incr x} {
      set sx [lindex $ssli $x]
      if {[llength [sets intersect $sx $sy]] > 0 }  {; continue; }
      set ss [sets union $sx $sy]
      set mm [minesinlist $ss]
      if {([minesinlist $ss] == $hm) && ([llength $ss] < $hc )} {
        set oss [sets difference $hcli $ss] 
        lappend hli [list {} [lsort -integer $oss] ]               ;# ���������� ������ ���������
        return  $hli
      }
      if {$ss ni $ssli} {; lappend ssli $ss; }
    }
    incr y
  }
  return $hli
}
;######################################################
;########### ��������� ��������� � ����� ##############
;######################################################

;########################## ��������� �� ��������� � GUI #########################
proc cellsize {} {return [expr 2*[font metrics TkDefaultFont -linespace]]}

proc coord {n} {
  global dimensions
  lassign $dimensions nx ny nz
# n = z*ny*nx + y*nx + x, where 
#   0 <= x < nx
#   0 <= y < ny
#   0 <= z < nz
#   0 <= n < nx*ny*nz
  set z [expr $n/($nx*$ny)] 
  set y [expr $n/$nx - $z*$ny] 
  set x [expr $n%$nx]
  return [list $x  $y  $z]
}

proc neighborhood {n} {
  global dimensions
  lassign $dimensions nx ny nz
  lassign [coord $n] x y z
  foreach a {-1 0 1} {
    foreach b {-1 0 1} {
      foreach c {-1 0 1} {
        if {($a != 0) || ($b != 0) || ($c != 0) } {
          lappend li [list [expr $x + $a ] [expr $y + $b ] [expr $z + $c ]]
        }
      }
    }
  }
  set res {}
  foreach r $li {
    lassign $r a b c
    if { (0 <= $a) && ($a < $nx) && (0 <= $b) && ($b < $ny) && (0 <= $c) && ($c < $nz)  } {
      lappend res [expr $c*$ny*$nx + $b*$nx + $a]
    }
  }
  return $res
}

proc dsize {} {
  global dimensions
  lassign $dimensions x y z
  return [expr $x*$y*$z]
}

proc genmines {range n} { ;# ���������� ������ ����� � ������ (range - ���������� �����, n - ���������� ���)
  set n1 [expr min($n,$range) - 1]
  return [lrange [lrandn $range] 0 $n1]
}

proc minesinlist {li} { 
  global minelist
  return [llength [sets intersect $li $minelist ]]
}

proc genhint0 {} { ;# ������� ������ ������ ����� ���������� � ����������� ����������� ��� ��-���������
  global minelist oclist
  set li [sets difference [intseq [dsize]] [concat $minelist $oclist]] ;# �������� ������ �������� �����
  if {[llength $li] == 0} {;return 0;}   ;# ���� ��� ������ ������� - ���������� �������
  set li [lsort -integer $li]
  set li1 [lmap x $li { minesinlist [neighborhood $x]}] ;# �������� ���-�� ��� � �������� ������� (li1 "����������" c li!!!)
  set tv [lindex [lsort -increasing -integer $li1] 0]   ;# ������� �����������
  return [lindex $li [lsearch $li1 $tv]]  ;# ���������� "������������" 
}

proc oclist_append { ocli args } { ;# ����� ������ �������� ����� �� ������ ������� � ����� �����. ������ � 0 ����������� ����������.
# ����������� ������ ���������� �� ������� �������� ��� ������� ����� � ����� ����� ��� - �������� �������� ��� ��������
  global minelist 
  while {[llength $args] > 0} {
    set args [lassign $args cell]
    if { ($cell ni $minelist) && ($cell ni $ocli) } {  
      lappend ocli $cell
      set nbh [neighborhood $cell]
      if { [minesinlist $nbh] == 0} {; lappend args {*}$nbh; }
    }
  }
  return $ocli
}

proc omlist_switch { i } { 
  global omlist
  if {$i ni $omlist} {
    lappend omlist $i
  } else {
    set omlist [lsearch -not -all -inline $omlist $i] ;# ������� $i
  }
}

proc restart_game {} {
  global oclist omlist gametime0 gametime errors hints
  set oclist {}   ;# ����, ������ ��� genhint0 ���������� ���� �������
  set oclist [oclist_append {} [genhint0]]
  set gametime0 [clock seconds]
  lassign { {} 0 0 0 } omlist gametime errors hints  
}

proc new_game {} {
  global mines minelist
  set minelist [genmines [dsize] $mines]
  restart_game
}
;########################## ��������� ��������� � GUI #########################

#--- mytimer with GUI --- begin
array set animationCallbacks {}
proc mytimer {w interval} {
  global animationCallbacks gametime0 gametime
  set procname [lindex [info level 0] 0]
  set animationCallbacks($w) [after $interval $procname $w $interval]
  set gametime [expr [clock seconds] - $gametime0]
  $w configure -text [clock format $gametime -format "%H:%M:%S" -timezone :UTC ] 
}
proc mytimer_start {w interval} {
  global animationCallbacks gametime0 gametime
  set gametime0 [clock seconds]
  set gametime 0
  set animationCallbacks($w) [after $interval mytimer $w $interval]
  bind $w <Destroy> {
    after cancel $animationCallbacks(%W)
    unset animationCallbacks(%W)
  }
}
#--- mytimer with GUI --- end

proc show_open_cell { i } {
  set w .gameframe
  set mines [minesinlist [neighborhood $i]]
  $w.$i configure -relief sunken -fg blue -text "$mines" 
  bind $w.$i <Button-2> "mouse2_show_nbh $i"
  bind $w.$i <Button-3> "mouse2_show_nbh $i"
}

proc show_oc {} {
  global oclist
  foreach i $oclist {; show_open_cell $i; } 
}
 
proc show_om {} {                        ;# �������, ����� ���������������, �� ����� �� ???????
  global omlist oclist cmine
  set w .gameframe
  set limit [dsize]
  for {set i 0} { $i < $limit } {incr i } {
    if {$i in $oclist} {; continue ;}
    if {$i in $omlist} {; $w.$i configure -text $cmine; continue ;}
    $w.$i configure -text ""
  }
} 

proc mouse1 {i} {
  global minelist   omlist oclist mines
  set limit [dsize]
  if {$i in $omlist} {; bell; return; }
  if {$i ni $minelist} {
    set oclist_old $oclist
    set oclist [oclist_append $oclist $i]
    foreach x [sets difference $oclist $oclist_old] {
      show_open_cell $x
    }
    if {[expr [llength $oclist] + $mines] >= $limit } {
      show_win
    }
  } else {
    save_game_to_autosave
    show_rip
  }
  renew_statusbar
}

proc mouse2_switch_mine {i} { ;# �������, ����� ���������������, �� ����� �� ???????
  global mines omlist
  omlist_switch $i
  show_om
  renew_statusbar
}

proc mouse2_show_nbh {i} {     ;# ������������� ������ ������� - ���������� ��� ������!!!!!!!!!!
  global bgcolor1 bgcolor2
  set limit [dsize]
  set w .gameframe
  for {set x 0} { $x < $limit } {incr x } {
    $w.$x configure -bg $bgcolor1
  }
  set li [neighborhood $i]
  foreach x [lappend li $i] {
    $w.$x configure -bg $bgcolor2
  }
}

#--- statusbar (GUI) --- begin
proc draw_statusbar {} {
  set w .statusbar
  label $w.timer -relief sunken -bd 1 -width 8 -anchor w -text ""  ;# -font "System 10"
  label $w.dimensions -relief sunken -bd 1 -text "" 
  label $w.mines -relief sunken -bd 1 -text "" 
  label $w.errors -relief sunken -bd 1 -text "" 
  label $w.hints -relief sunken -bd 1 -text "" 
  label $w.info -relief sunken -bd 1 -anchor w -text ""              
  pack $w.timer $w.dimensions $w.mines $w.errors $w.hints -side left -padx 2
  pack $w.info -side left -padx 2 -fill x -expand 1 

  mytimer_start $w.timer 1000
  bind $w.hints <Button-2> { console show }
  bind $w.hints <Button-3> { console show }
  bind $w.hints <Button-1> {
   .statusbar.info configure -text [ft "Hints: %s" [llength [bigHint shortlist]]] 
  }
}
proc renew_statusbar {} {
  global mines omlist errors hints dimensions;# oclist  
  set w .statusbar
  $w.mines      configure -text [ft "It is found %s mines of %s " [llength $omlist] $mines]
  lassign $dimensions x y z
  $w.dimensions configure -text [ft "Dimensions: %s " [format "%sx%sx%s=%s" $x $y $z [dsize]]]
  $w.errors     configure -text [ft "Errors: %s " $errors]
  $w.hints      configure -text [ft "Used hints: %s " $hints]
  $w.info       configure -text "" 
}
#--- statusbar (GUI) --- end

proc redraw_gameframe {} {
  global dimensions 
  lassign $dimensions nx ny nz
  set intsizeX [expr 1 + ($nx +1)*$nz]
  set intsizeY [expr 1 + $ny + 1 ]
  set cs [cellsize]
  set w .gameframe
  foreach x [winfo children $w] {; destroy $x;}

  set frameSizeX [expr $cs*$intsizeX]
  set frameSizeY [expr $cs*$intsizeY]
  $w configure -width $frameSizeX -height $frameSizeY

  for {set i 0} { $i < $nz } {incr i } {  ;# �������� �� ������
    set labtext [ft "Middle %s" $i]
    if {"$i" eq "0"} {set labtext [ft "Bottom"]}
    if {"$i" eq "[expr $nz-1]"} {set labtext [ft "Top"]}

    label $w.lab$i -text $labtext 
    set intx1 [expr 1 + ($nx + 1)*$i]
    place  $w.lab$i -x [expr $cs*$intx1] -y 0 -relwidth [expr $nx*(1./$intsizeX)] -relheight [expr 1./$intsizeY]
  }

  set limit [expr $nx*$ny*$nz]
  for {set i 0} { $i < $limit } {incr i } {
    lassign [coord $i] ix iy iz
    set intX [expr 1 + ($nx + 1)*$iz + $ix] 
    set intY [expr 1 + $iy] 
    label  $w.$i -relief raised -text ""   -borderwidth 4
    place  $w.$i -x [expr $cs*$intX] -y [expr $cs*$intY] -relwidth [expr 1./$intsizeX] -relheight [expr 1./$intsizeY]

    bind $w.$i <Button-1> "mouse1 $i"      
    bind $w.$i <Button-2> "mouse2_switch_mine $i"      
    bind $w.$i <Button-3> "mouse2_switch_mine $i"      

    bind $w.$i <Enter> ".statusbar.info configure -text \"[ft {�ell %s} $i]\""
    bind $w.$i <Leave> ".statusbar.info configure -text {}"      
  }
}

proc redraw_game {} {
  redraw_gameframe 
  renew_statusbar 
  show_om
  show_oc
}

;########### �������� ���������  ##############
proc show_rip {} {
  global errors
  incr errors

  set w .rip
  toplevel $w -bg red
  wm overrideredirect $w 1

  set sw [winfo vrootwidth $w]
  set sh [winfo vrootheight $w]
  wm geometry $w [format "%sx%s+0+0" $sw $sh]
  
  set c $w.c
  canvas $c
  place $c -in $w -relx 0.5 -rely 0.5 -anchor center
  bind $w <Button-1> "destroy $w"      
  bind $w <Key-space> "destroy $w"
  
  $c create arc  -25m -40m  25m  0m -width 3m -style arc -outline black -start 0 -extent 180
  $c create line -25m -20m -25m 30m -width 3m -fill black 
  $c create line  25m -20m  25m 30m -width 3m -fill black 
  $c create rect -35m  30m  35m 40m -width 3m 

  $c create text  0m  10m -anchor n -justify center -font {areal 40 bold}  -text [ft "R.I.P." ]
  $c create image 0m -15m -image sapper128 
  
  $c move all 50m 50m
  $c create rect 2 2 100m 100m -width 1

  lassign [$c bbox all] x1 y1 x2 y2
  $c configure -width [expr $x2 - $x1] -height [expr $y2 - $y1]
  
  raise $w
  focus $w
  bell
}
  
;########### �������� ��������  ##############
proc show_win {} {
  set w .win
  toplevel $w -bg green
  wm overrideredirect $w 1
  
  set sw [winfo vrootwidth $w]
  set sh [winfo vrootheight $w]
  wm geometry $w [format "%sx%s+0+0" $sw $sh]
  
  set c $w.c
  canvas $c
  place $c -in .win -relx 0.5 -rely 0.5 -anchor center
  bind $w <Button-1> "destroy $w"      
  bind $w <Key-space> "destroy $w"

  $c create image 0m -25m -image sapper128 
  
  $c create text  0m -10m -anchor n -justify center -font {areal 50 bold} -fill green -text [ft "You win!"]  -tag wintext 
  
  global errors hints gametime
  set stat [ft "Statistics\n\n Game time: %s\n Errors: %s\n Used hints: %s " \
                                          [clock format $gametime -format "%H:%M:%S" -timezone :UTC] \
                                                       $errors \
                                                                        $hints]
  $c create text 0m 15m -anchor n -justify center -text $stat
  
  for {set i 0} {$i < 81} {incr i} {
    set tv [lindex {magenta white green white red white blue white} [expr $i % 8]]
    after [expr $i*200] "catch \"$c itemconfigure wintext -fill $tv\""
  }

  $c move all 50m 50m
  $c create rect 2 2 100m 100m -width 1 -tag rectangle

  lassign [$c bbox all] x1 y1 x2 y2
  $c configure -width [expr $x2 - $x1] -height [expr $y2 - $y1]

  raise $w
  focus $w
  bell
}

;######################################################
;########### ��������� ��������� � ����  ##############
;######################################################
proc SaveGame {} {
  set fname [tk_getSaveFile -defaultextension ".sav" -filetypes { {Save .sav}  {All *.*} } -confirmoverwrite 1 -initialdir [workdir]]
  if {$fname eq ""} {; return; }
  save_game_to_file $fname 
}

proc LoadGame {} {
  set fname [tk_getOpenFile -filetypes { {Save .sav} {All *.*} } -initialdir [workdir]]
  if {$fname eq ""} {; return; }
  load_game_from_file $fname 
  redraw_menu
  redraw_game
}

proc NewGame {} {
  global dimensions mines minelist oclist omlist 
  set w .newgame

  global _Gx _Gy _Gz
  lassign $dimensions _Gx _Gy _Gz
  toplevel $w 
  wm title $w [ft "New game"]
  labelframe $w.f1   -text [ft "Dimensions"]
  pack $w.f1 -side top -ipadx 5m 

  foreach i { x y z } {
    frame $w.f1.f$i 
    pack $w.f1.f$i -side top
    label $w.f1.f$i.lab$i -text " [string toupper $i]: "
    pack  $w.f1.f$i.lab$i -side left
    spinbox $w.f1.f$i.spin$i -from 1 -to 100 -width 3 -justify right -relief sunken \
            -font TkDefaultFont -textvariable _G$i
    pack  $w.f1.f$i.spin$i -side right -pady 2m 
  }

  labelframe $w.f2  ;# -text Mines
  pack $w.f2 -side top  -fill x ;# -ipadx 5m 
  label $w.f2.lab -text [ft "Mines: "]
  pack  $w.f2.lab -side left
  spinbox $w.f2.mines -from 1 -to 999 -width 4 -justify right -relief sunken \
            -font TkDefaultFont -textvariable mines
  pack  $w.f2.mines -side top -pady 2m ;# -fill x 
                          
  labelframe $w.f3 
  pack $w.f3 -side bottom -fill x
  button $w.f3.b1 -text [ft "Cansel"]  -command { destroy .newgame }
  pack $w.f3.b1 -side left
  button $w.f3.b2 -text [ft "Start"]  -command \
        {; set dimensions [list $_Gx $_Gy $_Gz]; new_game; redraw_game; destroy .newgame; }
  pack $w.f3.b2 -side right

  wm geometry $w +[winfo pointerx $w]+[winfo pointery $w]
  focus $w
  grab $w
  tkwait window $w
}

proc Hint {} { 
  global hints
  global oclist omlist minelist
  set li [bigHint "shortlist" 25]
  if {[llength $li] == 0} {
    tk_messageBox -type ok -icon info -title [ft "Hint"] -message [ft "You have no hints now!"]
    return
  } else {
    set hint [lindex $li 0]
  }
  lassign $hint om oc 
  foreach x $oc {; .gameframe.$x configure -bg {lawn green}; }
  foreach x $om {; .gameframe.$x configure -bg  #ff5050;     }     ;# red  

  incr hints 
  renew_statusbar
  bell
}

proc CheckGame {} {
  global mines omlist minelist
  set serious_errors [llength [sets difference $omlist $minelist]]

  puts  [time  { set li [bigHint x 25] } 1 ]
# ������� ��������� ����:
  set li1 {}
  foreach x $li {
    lappend li1 {*}[lindex $x 0]
  }
  set mm [llength [lunique $li1]]
# ����������� ��� �������� � ������ ����
  incr mm [expr [llength $omlist] - $serious_errors ]
#----- ���������
  if {$mm < $mines} {
    tk_messageBox -type ok -icon warning -title [ft "Warning"] -message \
       [ft "Most likely this game has no purely logical decision -\nI have found only %s mines of %s.\nBut you can use \"Not-logical hint\" (see the menu of a game)." $mm $mines]
  } else {
    tk_messageBox -type ok -icon info -title "Info" -message [ft "This game has purely logical decision."]
  }
  bell
}

proc SeeSource {filename} { ;# ���������� filename ��� �������� tcl/tk �� ����������� ���� .view (�������� ��� ���� � ��������?) 
  set w .view
  if { [winfo exists $w] } { return }
  set tv1 [open $filename]; set sourcecode [read $tv1]; close $tv1 ;# ��������� ���� �������
  toplevel $w 
  wm title $w [ft "Source code"]
  text $w.t -wrap char -yscroll "$w.s set"
  scrollbar $w.s -orient vertical -command "$w.t yview" ;#-

  set sourcelist [split [string map {\r ""} $sourcecode] \n]

  set ch 0           ;# ������� �����
  $w.t tag configure numfont  -font "systemfixed 8 bold" 
  foreach tv1 $sourcelist {
    set numstr [format "%04d. " [incr ch]]
    $w.t insert end $numstr numfont
    $w.t insert end "$tv1\n"
  }
  $w.t config -state disabled
  pack $w.s -side right -fill y -padx {0 2} -pady 2
  pack $w.t -fill both -expand 1 -pady 2 -padx {2 0}
}

image create photo sapper128 -data {
  R0lGODlhgACAAKUyAAAA/gAA/wEB/pwULigrpJwVLiwunp8cLS4u0Z8dLS8v0DAwz6IjKzg4xzk5
  zKUqKqUrKkRIZ0JCw0xRQUxSQFJXPlRaOFdcPLNKJF1jL19lKbZRIrhYIblZIXd3iICAgIiId8yG
  GM+NFtKUFdWcFMfHOMjIN9DQL9HRLtPSM+vQCezRCe7YCNzbKu/ZCP7+Af/+AP//AP//////////
  /////////////////////////////////////////////yH5BAEKAD8ALAAAAACAAIAAAAb+wJ9w
  SCwaj8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum8/otHrNbrvf8Lh83rzY73e63ojv+/0/dntv
  f4WGfYGDZoeMjXiKYY6SkxeQWZSYk5ZUmZ2am06eop+gSaOnkqV8qKyOqoGtsY2lsrWMm7a5h4q6
  vYV7vsGAcsLFiHDGyXluys2Ca87RatHSaNTUZ9fXi9rYY93aYuDdYOPkXubg6OnnWuzj7u/ql/Lz
  VvXwV/j5nPv2U/74RQkoEArBgqEOtnuiEOGShv+YQIz4cGK4hBYv1sm4bSPHjhU/VgspcqSSkt5O
  okyJZCXLIy6dkYyZTCVNZTZvGpup01f+zp7CTAHd2XJoMaFGgyEtVIGC06dQo0qdaiGCgQgWpmrd
  CtWCBgIRMkzgSlZqhVlUYqhdy7atW7cpJASYKyHF27t41zoIAGBui7yA3YYJTHjt3MNzCwdWIABx
  ABOKAw+OjJexYwEKKL8t4XhuA813xYB+2/nw6LUvQJQOcLqt6NZqPTTuLMAD7NW1Yat9DfvD6gAf
  bq8Orpt3a9/DhZcmDtv4ac6rS8BuEF13DOenf+uWXdr69cmwX5yg/eGFbvLmi2MfvcBxZt0vUCAW
  gMA7Ge8xSlBvIN37CxOqNWCbfffhZyBb6eFnxoEMNrhggxASWGCEFJ72YIUYUnZGhhz+EoZGhyDi
  lUaIJK6lRoklrgEaDCiyxWJhbGgWAgstwrACBpG1QdkIDHSgQogsjnBAAoq5seMDDzCwQQwvYsjC
  ABA8QCSMRiq2wgFIQjDACBiq0AEDWS7pIRyKweAClFke4OOBLHoJJpJKUklmmTFs8CacHYigW5sk
  FHCnljTKOWdkT0aJ5AMQqDnCj3TGoEKfd8K5ZJOA6UFZkFgeeigDCXQwwgh63iXCCH0mEGmWHDAq
  6Bya2filprAiOmQCtNZ6gKGxIrqlhoqM5uWtuQYrrK5cUiqZJa2N4OewwyaaJ5O8InsanwUAy2yS
  nYY6miqwvThqqbXW6umizb3yA35Nxh5o7hAtFrkuu+1W+i4R8Yo4rxH1snVvEvl+ty+/Lf7rBIkC
  R8FhwWlFiLAW6i3sxbYOf2hixBRXbPHFGGes8cYcd+zxxyCHLLIRQQAAOw==
}

image create photo foolquest -data {
R0lGODlhugCAAMIFAAAAAAAA//8AAACAANTQyP///////////yH5BAEKAAcALAAAAAC6AIAAAAP+eLrc
/jDKSau9OOvNu/9gKI5kaZ5oqq5s675wLM90bTtAfu98m/+AnnAYAhqJpYByyWw6n0pkxEiVjqDY7NKK
o1a5H634CVZ4z2XPeL3lnn+Hb1rDZlvfOrN8fqmvpXhdQHwZfmNEeEEPe4QThmJCiRKMjRQCl5iXlQyB
FJSbEJmZoJ0VXqCWogIwj0+SOFNHqBGqmi+tTK+LiruDsw+1q7e4AaUQeb1wvw3BMbi6x8rJyMsHzayG
0NHSvHqy1bUyfolRnqcNn5vX2Gt4TKZo6N+z68Ni5O/wlOmN4UOX2izEm9ZNnb8eAgJiOEewYL+DPIxx
GChImkFRkSR2CDL+ryI1QsFCihxJslaiUQJ9ybO4kiWfkjBjwsQTchLFOCoJVpLJs2cmmiMbusQ51FvO
ND6TzgQatGW6jk4dclFKNeTJmJyO2lT2kWhRJFXD/nzTM2vXrfugWhEb9ipPswUZGoXKTwhbqm59LhiU
KG7ar0RqstBVz9rBvh0Z3VwrOIXEwgmBYJqLuEtUrWBFOn5cj+zeN3PNgS5D0oTCzsbUYv48mnTJEQoV
rKusTypltYxfg9DIYPYgTKqdCgUsRaaH2At85xnrEhld3q6xbkDe2983gPO09QVVdiF0Bz8nh5ZtxJZX
uefrgqmpOeViCcBzmJdFBeXty6tfHmyPFv3+BOySsXYGc7X5dxFGyTXmkXrAAPjRgPH9MMCEFFY4AHUg
QWRYYQIyGIqD1MgRIQACWFghhg+pAp6C6eWXyoh+STNiiSZeiMeE1cimoY7XoGjZhjDCpcOEkf1wiYWJ
mLgMhwmalNon5amSWF+YUJhkjTgOgWWNQNrXoChPtjZXMFOSxdwZW1q4Q5pYssjMmQwhFqN8UuY0oZkg
UsEmlzLsuSWTb0IjZ4iSISjLiVHCqKefSr7A6J87LlimShQlasuiaeap1qMDtMBpm7VQGAuEwqTXUIcI
nrcnho+q8CmkqlQoaaHz5bdHokICsKWPCjCKwquwitJoXpPh1iEv9ZX+ml6NV2XgZwnABpsJs0w5Sdx5
OkZ5bA6IMrXBniNwWkEwJpZi1bWm5snanbp2S6p4zrIZQqvjkmvlk9ba5tGGVyJZrZcYgOuBrxfYa6OY
ROXrHp2KSujvaIBWIG8Hz2ZAbpJO1YnueXAC4S56EVMw8bcjFxzOjaIeC6aLQq7s8b0I85gqyVhyUDIG
+XrR6JhwLkzizB+n42bAW2pwM84dG1GzyopukyzQB79XnYoDL0201Rq4fEqaLfc8XHkMRG3gilRXzacF
XHfQsEVp86zurPVty/KbZduMtch3W1zkTW1j2zE6chvZ4txkz2z32RP0rUFsfR/R8Nc6YKx3pFf+75x4
3t5JvUDRHfI4qJFa69zpBiGjjXgEnE/33eb3ytp50nhq+nKWWVNeuZqXn+5z3AVKmCuQcsLucMoclC6x
7g5gPoFbot0kS+jP7y067sXbfoHyrCPfn6XNi/mFxtRIP73rahuPt+XJa++0F4YvyG52vvB32yDYm1z3
4dQ/UH+ulpo3qiJBE5LMqKaL/dXrfjTLXwMMODjQIVA4vYIZoWREJokw8EXtixf6wqa+wCmidHaS4Jx4
AT72KesAF/yP9Y63wewpED/KMF4IRSjAJjFvgR203wOvpz7lQUOGbPsYBA1DrLskxWwvdCH5+MeSxpzl
Gw97kEWKaMQjUqz+h6fjTTMqpZIoOqQbVKyiUq7Ywgju7BUFyZkcvhDF7fVPjGEhYxJRyCc0UhB6cTqC
ECEHRzHKcYk4LFdrrgO9+7TofUUJYx+p8kfipS9o9DkH+BY0v78MZJFGbCTtHim2L1wydNPwYLtG18Cu
YFIsSAQkB+0IsjOtj4mjrORZjJbDGmCvk76ImQ2/8pxRitBDpivjmtTXCc215H9SoZYuU+nIHtRvIGN7
ZSg5iUvC0VKYtuwgpigziX1Nk4OIjCYIUscDchKEdsZiCZSGEk5gMnOTw8QmZYjXy64oRivV3Ng4U7gC
c66vXceS22W4CUN97rOWLvDnOdGZSwPJqaD++kqBQmMw0eQtSpSzlOUI9TSDijoqhaLzYO+miLJsIlSi
/OxkypY5Koiq8qP8JIHiRHZRgn6gX/Dsk0d/tVP9bdNUN0XZS2FwNFfNVGI/NdZIZzdUih71BEVFqtJW
ak2XxpSnT5VpVKXKVKDubqpXRWlW57XVr3rxq0w9KUx7ir+wLhBT7eweXN1q1LJec6wBm6tSccrWjlbs
nWrtnRelwte+0oBed7Wr6tI6qfEpNp5/ZWFkYQNWREIyrY8tZ7Q229TdVPZCl8WsYZ3J2c2qwLG//Gxm
kVDaT/lAtYXF6xxaKzAXYCq2o00DbQMrgirglq5l2G1OXzAoxOZIApwp3YGcJntcydY2IqtrrnTRGtHp
Wrd3183ucTKq3e5697vgDa94x6uCBAAAOw==
}

proc About {} {
  global scriptname sapper128 foolquest
  set w .about
  catch {destroy $w}
  toplevel $w 
  wm title $w [ft "About program"]
  frame $w.f 
  pack $w.f -fill x
  set iw [image width  sapper128]
  set ih [image height sapper128]
  canvas $w.f.c  -width $iw -height $ih 
  pack $w.f.c -pady {4 0} -side left    
  $w.f.c create image 2 2 -anchor nw -image sapper128
  label $w.f.label -padx 10 -text \
     "\nFoolQuest Minesweeper\nVer 0.81\n[clock format [file mtime $scriptname]]\n" 
  pack $w.f.label -side left -fill x -expand 1
  set iw [image width  foolquest]
  set ih [image height foolquest]
  canvas $w.f.c1  -width [expr $iw + 20] -height $ih 
  pack $w.f.c1 -pady {4 0} -side left    
  $w.f.c1 create image 2 2 -anchor nw -image foolquest

  frame $w.f2 
  pack $w.f2 -fill x
  button $w.f2.b -width 8 -text [ft "Ok"] -command "destroy $w"
  pack $w.f2.b -pady {10 10} 

  frame $w.f3 
  pack $w.f3 -fill x -side top
  label $w.f3.lab  -text [ft "Try to play without errors and hints\nUse right mouse button on open cells" ]
  pack $w.f3.lab -pady {10 10} 

  wm geometry $w +[winfo pointerx $w]+[winfo pointery $w]
  raise $w
}

proc Autoplay {} {
  global minelist   omlist oclist mines  __Autoplay_Flag__
  set __Autoplay_Flag__ 0

  set w .statusbar.info
  $w configure -text [ft "Click here to stop autoplay!" ]
  bind $w <Button-1> "set __Autoplay_Flag__ 1"      
  bind $w <Button-2> "set __Autoplay_Flag__ 1"     
  bind $w <Button-3> "set __Autoplay_Flag__ 1"      

  set li [bigHint all 25]

# ������ li �� ����������� ��������� (- "�������" ����� "�������")
  set li1 {}
  foreach x $li {
    lassign $x om oc
    if {([llength $om] > 0) && ([llength $oc] > 0)} {
      lappend li1 [list $om {}]
      lappend li1 [list {} $oc]
    } else {
      lappend li1 $x
    }
  }
  set li [lunique1 $li1]
# ��������...
  foreach x $li {
    lassign $x om oc
    set oclist_old $oclist
    set oclist [oclist_append $oclist {*}$oc]
    foreach y [sets difference $oclist $oclist_old] {
      show_open_cell $y
      update idletasks 
    }
    foreach y $om {
      if {$y ni $omlist} {; lappend omlist $y;}
      show_om
      update idletasks 
    }
    renew_statusbar
    $w configure -fg red -text [ft "Click here to stop autoplay!" ]
    if {$__Autoplay_Flag__} {; break; }

    set flag 0
    after 200   { set flag 1 }
    vwait flag
  }
  bell
  bind $w <Button-1> {}      
  bind $w <Button-2> {}     
  bind $w <Button-3> {}      
  $w configure -fg black -text ""
}

proc redraw_menu {} {
  global dimensions mines scriptname
  lassign $dimensions nx ny nz
  set w .menu
  foreach x [winfo children $w] {; destroy $x;}

  menu $w.menu -tearoff 0

  set m $w.menu.file
  menu $m  -tearoff 0
  $w.menu add cascade -label [ft "Game"] -menu $m -underline 0
  $m add command -label [ft "New game"] -command { NewGame }
  $m add command -label [ft "Restart game"] -command { restart_game; redraw_game; }
  $m add separator
  $m add command -label [ft "Load game from autosave"] -command { load_game_from_autosave; redraw_game;}
  $m add command -label [ft "Load game from..."] -command { LoadGame }
  $m add command -label [ft "Save game as..."] -command { SaveGame }
  $m add separator
  $m add command -label [ft "Check for logical decision"] -command { CheckGame }
  $m add command -label [ft "Logical hint"] -command {  Hint  }
  $m add command -label [ft "Not-logical hint (open one empty cell)"] -command { ; global hints oclist; incr hints; renew_statusbar; set oclist [oclist_append $oclist [genhint0]]; show_oc; show_om; }
  $m add separator
  $m add command -label [ft "Autoplay"] -command { Autoplay }
  $m add separator
  $m add command -label [ft "Exit"] -command {exit}

  set m $w.menu.info
  menu $m -tearoff 0
  $w.menu add cascade -label [ft "Info"] -menu $m -underline 0
  $m add command -label [ft "About program"] -command { About }
  $m add command -label [ft "See code"] -command "SeeSource $scriptname"
}

proc check_tcl_ver {} {
  global tcl_version
  if { $tcl_version < 8.6} {
    tk_messageBox -type ok -icon info -title [ft "Info"] -message [ft "You need Tcl/Tk version >= 8.6."]
    exit 
  }
}
;###############################################################
;## ������ �������������� ��������� � ������������ ����������
;###############################################################
wm title . [ft "FoolQuest Minesweeper"]
wm withdraw .  ;# ������� �������� ���� �� ����� ��������������� ���������,
wm iconphoto . -default sapper128 ;# ������ ������ �� ����

check_tcl_ver

if {[file exists [autosavename]]} {
  load_game_from_autosave
} else {
  new_game
  save_game_to_autosave
}

frame .menu
. configure -menu .menu.menu
redraw_menu 

frame .statusbar
pack .statusbar -side bottom -fill x -pady 2
draw_statusbar

labelframe .gameframe
pack .gameframe 
redraw_game

bind . <Destroy> {if {"%W" == "."} {save_game_to_autosave} }

wm state . normal ;# ���������� �������� ����
raise .
